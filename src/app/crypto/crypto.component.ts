import { Component, Input, OnInit } from '@angular/core';
import { CryptoPrice } from '../model/crypto-price';

@Component({
  selector: 'crypto',
  templateUrl: './crypto.component.html',
  styleUrls: ['./crypto.component.css']
})
export class CryptoComponent implements OnInit {


  @Input()
  price : CryptoPrice

  constructor() { }

  ngOnInit(): void {
  }

}

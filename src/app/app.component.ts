import {AfterViewInit, Component, ElementRef, OnInit, QueryList, ViewChild, ViewChildren} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http'
import {Observable, timer} from 'rxjs';
import { CryptoCompareService } from './services/crypto-compare.service';
import { CryptoPrice } from './model/crypto-price';
import { map, concatMap, filter, take, switchMap } from 'rxjs/operators'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {


  price$ : Observable<CryptoPrice>;

  // courses;

  constructor(private cryptocompare: CryptoCompareService) {

  }

  ngOnInit() {

    this.price$ = timer(1, 3000).pipe( 
      switchMap(() => this.cryptocompare.price())
   );
  }



}
